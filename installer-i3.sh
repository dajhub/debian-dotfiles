#!/bin/bash

# Install packages after installing base Debian with no GUI

# Updates to the latest versions of the packages and dependencies
sudo apt update

# XORG display server installation
sudo apt install -y xorg xbacklight xbindkeys xvkbd xinput xorg-dev

# i3 - PACKAGES
sudo apt install -y brightnessctl numlockx i3 i3status i3lock dmenu picom polybar rofi feh 

# Network
sudo apt install -y network-manager

# Python installed == python3-i3ipc needed for autotiling
sudo apt install -y python3-pip python3-i3ipc

# Install file manager - dolphin
#sudo apt install -y dolphin breeze-icon-theme qt5ct qt5-style-kvantum ark

# File Manager (eg. pcmanfm,thunar)
#sudo apt install -y pcmanfm thunar xfce4-settings xfce4-power-manager xfce4-terminal xarchiver
sudo apt install -y thunar xarchiver

# LXQT authetication agent for PolicyKit
sudo apt install -y lxpolkit


# Installs stow for symbolic links
sudo apt install -y stow

# Fonts
sudo apt install -y fonts-font-awesome fonts-inter

# Audio
sudo apt install -y pipewire-audio wireplumber pipewire-pulse pipewire-alsa libspa-0.2-bluetooth
systemctl --user --now enable wireplumber.service

# Browsers
sudo apt install -y chromium

# Install packages
sudo apt install -y wget kitty lxappearance nitrogen fish

# Install nala as a replacement for apt
sudo apt install -y nala

# Packages to be made aware of bugs in the unstable branch
sudo apt install -y apt-listbugs apt-listchanges

# Neofetch/HTOP
sudo apt install -y neofetch htop

# Printing
sudo apt install -y system-config-printer hplip cups
sudo systemctl start cups.service
sudo systemctl enable cups.service


# Install code - source https://code.visualstudio.com/docs/setup/linux
sudo apt-get install -y gpg
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -D -o root -g root -m 644 packages.microsoft.gpg /etc/apt/keyrings/packages.microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg

sudo apt install -y apt-transport-https
sudo apt update
sudo apt install -y code

# Install OnlyOffice - source https://helpcenter.onlyoffice.com/installation/desktop-install-ubuntu.aspx

mkdir -p -m 700 ~/.gnupg
gpg --no-default-keyring --keyring gnupg-ring:/tmp/onlyoffice.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys CB2DE8E5
chmod 644 /tmp/onlyoffice.gpg
sudo chown root:root /tmp/onlyoffice.gpg
sudo mv /tmp/onlyoffice.gpg /usr/share/keyrings/onlyoffice.gpg

echo 'deb [signed-by=/usr/share/keyrings/onlyoffice.gpg] https://download.onlyoffice.com/repo/debian squeeze main' | sudo tee -a /etc/apt/sources.list.d/onlyoffice.list

sudo apt-get update

sudo apt-get install -y onlyoffice-desktopeditors


# Install sddm without all of plasma. Installing sddm so that, if needed, hyprland (uses Wayland) can be installed later
#sudo apt-get install -y --no-install-recommends sddm
#sudo systemctl enable sddm

# Dependency for Corners theme (SDDM)
#sudo apt install -y qtquickcontrols2-5-dev libqt5svg5 qml-module-qtquick-window2 qml-module-qtquick-controls2 qml-module-qtgraphicaleffects

# Install slick-greeter
sudo apt install -y slick-greeter
sudo systemctl enable lightdm

# BETTERLOCKSCREEN - source https://github.com/betterlockscreen/betterlockscreen?tab=readme-ov-file
# Install dependencies for i3lock-color
sudo apt install -y autoconf gcc make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util0-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev
# Install i3lock-color
git clone https://github.com/Raymo111/i3lock-color.git
cd i3lock-color
./install-i3lock-color.sh
# Additional dependencies for Betterlockscreen
sudo apt install -y imagemagick
# Install Betterlockscreen
wget https://raw.githubusercontent.com/betterlockscreen/betterlockscreen/main/install.sh -O - -q | bash -s user

# Set fish as default shell
chsh -s /usr/bin/fish

sudo apt autoremove

printf "\e[1;32mFinished!\e[0m\n"
