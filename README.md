[TOC]

# debian-dotfiles-i3

Assumes a minimal install of debian stable with no desktop.  When completed reboot into terminal.

## Add Yourself to Sudoers File

Need to do this so that you can use `sudo apt...`

```bash
su
apt install sudo micro
micro /etc/sudoers
```
---

**Note:** If `micro /etc/sudoers` does not work then may need to install **sudo**, i.e.

```bash
sudo apt install sudo
```

---

In sudoers file, scroll down to this section:

```bash
## User privilege specification
##
root ALL=(ALL:ALL) ALL
```

Below the last entry (above) add the following, changing **david** to your username,

```bash
david ALL=(ALL) ALL
```

The sudoer file should now look as follows (with a different username):

```bash
## User privilege specification
##
root ALL=(ALL:ALL) ALL
david ALL=(ALL) ALL
```

To get back to user level, i.e. exit su, press `Ctrl`+`D`.


## Moving to Debian Unstable

To move the stable to unstable

```bash
sudo micro /etc/apt/sources.list
```

In the `sources.list` there needs to be only two entries:

```bash
deb http://deb.debian.org/debian/ unstable main non-free-firmware
deb-src http://deb.debian.org/debian/ unstable main non-free-firmware
```

Alternatively, add to the sources list testing and stable and then pinpoint the priority in `/etc/apt/preferences.conf`.  Means you can have some packages from testing or stable if you desired.

1. Sources.list, i.e. `sudo micro /etc/apt/sources/list`

```bash
deb http://deb.debian.org/debian/ unstable main non-free-firmware

deb http://deb.debian.org/debian/ testing main non-free-firmware

deb http://deb.debian.org/debian/ stable main non-free-firmware

deb http://security.debian.org/debian-security testing-security main non-free-firmware
```
2. In preferences.conf, i.e. `sudo micro /etc/apt/preferences.conf`

```bash
Package: *
Pin: release a=testing
Pin-Priority: 100

Package: *
Pin: release a=unstable
Pin-Priority: 1000

Package: *
Pin: release a=stable
Pin-Priority: 1
```


Next, run

```bash
sudo apt update && sudo apt full-upgrade
```


## Preparing to use Install Script

In a terminal first install git.

```bash
sudo apt install git
```
To run the script:

```bash
git clone https://gitlab.com/dajhub/debian-dotfiles
cd debian-dotfiles
./installer-i3.sh
```

Check the content of the script before running!

## Autotiling

Source: https://github.com/nwg-piotr/autotiling

To run script to install:

```bash
cd debian-dotfiles/scripts
./autotiling.sh
```

## Theming: Folders, Wallpapers & SDDM Theme

These will provide the backgounds for feh to use and the folders will enable the theming. There are two options available.  See: https://gitlab.com/dajhub/i3-dotfiles#copying-files-folders--wallpapers. 



## Touchpad not working

If the touchpad is not working, then this may help solve (again, worth checking the script first):

```bash
cd debian-dotfiles/scripts
./touchpad.sh
```


## Themes used

- Utterly Nord Light: https://store.kde.org/p/1917093/
- Orcis Light Compact: https://www.gnome-look.org/p/1357889 
- Papirus icon theme: https://www.pling.com/p/1360398/ 
