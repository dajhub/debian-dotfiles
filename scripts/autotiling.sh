#!/bin/bash

# Installs autotiling for i3 - source: https://github.com/nwg-piotr/autotiling
cd
git clone https://github.com/nwg-piotr/autotiling.git
cd autotiling/autotiling
cp main.py ~/autotiling/autotiling/autotiling
chmod u+x autotiling
sudo mv autotiling /bin/
cd
rm -rf autotiling